package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final PositiveInteger powerGridRequirements;

	private final PositiveInteger capacitorConsumption;

	private final PositiveInteger optimalSpeed;

	private final PositiveInteger optimalSize;

	private final PositiveInteger baseDamage;

	public AttackSubsystemImpl(String name, PositiveInteger powerGridRequirements, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.powerGridRequirements = powerGridRequirements;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powerGridRequirements,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, powerGridRequirements, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridRequirements;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier;
		if (target.getSize().value() >= this.optimalSize.value()) {
			sizeReductionModifier = 1;
		}
		else {
			sizeReductionModifier = (double) target.getSize().value() / this.optimalSize.value();
		}
		double speedReductionModifier;
		if (target.getCurrentSpeed().value() <= this.optimalSpeed.value()) {
			speedReductionModifier = 1;
		}
		else {
			speedReductionModifier = (double) this.optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
		}
		int damage = (int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier));
		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
