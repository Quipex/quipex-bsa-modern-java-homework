package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger powerGridOutput;

	private final PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powerGridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powerGridOutput = powerGridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name cannot be empty: '" + name + "'");
		}
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem consideredAttackSubsystem) throws InsufficientPowergridException {
		this.attackSubsystem = subsystemAfterFitting(consideredAttackSubsystem);
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem consideredDefSubsystem) throws InsufficientPowergridException {
		this.defenciveSubsystem = subsystemAfterFitting(consideredDefSubsystem);
	}

	private <T extends Subsystem> T subsystemAfterFitting(T consideredSubsystem) throws InsufficientPowergridException {
		if (consideredSubsystem == null) {
			return null;
		}
		Subsystem otherSubsystem = getOppositeSubsystem(consideredSubsystem);
		int otherSubsystemPowerConsumption = gridPowerConsumptionOf(otherSubsystem);
		int consideredSubsystemPowerConsumption = consideredSubsystem.getPowerGridConsumption().value();
		int powerGridLeft = this.powerGridOutput.value() - otherSubsystemPowerConsumption
				- consideredSubsystemPowerConsumption;
		if (powerGridLeft >= 0) {
			return consideredSubsystem;
		}
		else {
			throw new InsufficientPowergridException(-powerGridLeft);
		}
	}

	private <T extends Subsystem> Subsystem getOppositeSubsystem(T consideredSubsystem) {
		Subsystem opposite;
		if (consideredSubsystem instanceof AttackSubsystem) {
			opposite = this.defenciveSubsystem;
		}
		else if (consideredSubsystem instanceof DefenciveSubsystem) {
			opposite = this.attackSubsystem;
		}
		else {
			throw new RuntimeException("Unknown type: " + consideredSubsystem.getClass().getTypeName());
		}
		return opposite;
	}

	private int gridPowerConsumptionOf(Subsystem subsystem) {
		return subsystem == null ? 0 : subsystem.getPowerGridConsumption().value();
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return new CombatReadyShip(this.name, this.size, this.speed, this.shieldHP, this.hullHP, this.capacitorAmount,
				this.capacitorRechargeRate, this.attackSubsystem, this.defenciveSubsystem);
	}

}
