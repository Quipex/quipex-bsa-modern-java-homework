package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private final PositiveInteger size;

	private final PositiveInteger speed;

	private PositiveInteger currentShieldHP;

	private final PositiveInteger maxShieldHP;

	private PositiveInteger currentHullHP;

	private final PositiveInteger maxHullHP;

	private PositiveInteger currentCapacitorAmount;

	private final PositiveInteger maxCapacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final AttackSubsystem attackSubsystem;

	private final DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger size, PositiveInteger speed, PositiveInteger maxShieldHP,
			PositiveInteger maxHullHP, PositiveInteger maxCapacitorAmount, PositiveInteger capacitorRechargeRate,
			AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.size = size;
		this.speed = speed;
		this.maxShieldHP = maxShieldHP;
		this.maxHullHP = maxHullHP;
		this.maxCapacitorAmount = maxCapacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.currentHullHP = maxShieldHP;
		this.currentCapacitorAmount = maxCapacitorAmount;
		this.currentShieldHP = maxShieldHP;
	}

	@Override
	public void endTurn() {
		capacitorRecharge();
	}

	private void capacitorRecharge() {
		PositiveInteger actualRecharge = calculateRegenValue(this.currentCapacitorAmount, this.maxCapacitorAmount,
				this.capacitorRechargeRate);
		this.currentCapacitorAmount = PositiveInteger.of(this.currentCapacitorAmount.value() + actualRecharge.value());
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		int capacitorEnergyAfterAction = energyAfterUtilizingSubsystem(this.attackSubsystem);
		if (capacitorEnergyAfterAction >= 0) {
			this.currentCapacitorAmount = PositiveInteger.of(capacitorEnergyAfterAction);
			return Optional
					.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
		}
		else {
			return Optional.empty();
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		CombatReadyShip target;
		if (!(attack.target instanceof CombatReadyShip)) {
			throw new IllegalArgumentException("Target must be an instance of CombatReadyShip");
		}
		else {
			target = (CombatReadyShip) attack.target;
		}
		attack = target.defenciveSubsystem.reduceDamage(attack);
		boolean isDestroyed = target.applyDamageAndTellIfDestroyed(attack.damage);
		if (isDestroyed) {
			return new AttackResult.Destroyed();
		}
		else {
			return new AttackResult.DamageRecived(this.attackSubsystem, attack.damage, target);
		}
	}

	public boolean applyDamageAndTellIfDestroyed(PositiveInteger damage) {
		int shieldHPLeft = this.currentShieldHP.value() - damage.value();
		if (shieldHPLeft <= 0) { // shield was destroyed
			this.currentShieldHP = PositiveInteger.of(0);
			int penetratedDamage = -shieldHPLeft;
			int hullHPLeft = this.currentHullHP.value() - penetratedDamage;
			if (hullHPLeft <= 0) { // hull was destroyed
				this.currentHullHP = PositiveInteger.of(0);
				return true;
			}
			else {
				this.currentHullHP = PositiveInteger.of(hullHPLeft);
				return false;
			}
		}
		else {
			this.currentShieldHP = PositiveInteger.of(shieldHPLeft);
			return false;
		}
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		int capacitorEnergy = energyAfterUtilizingSubsystem(this.defenciveSubsystem);
		if (capacitorEnergy >= 0) {
			this.currentCapacitorAmount = PositiveInteger.of(capacitorEnergy);
			RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
			PositiveInteger shieldHpRegenerated = regenerateShield(regenerateAction);
			PositiveInteger hullHpRegenerated = regenerateHull(regenerateAction);
			return Optional.of(new RegenerateAction(shieldHpRegenerated, hullHpRegenerated));
		}
		else {
			return Optional.empty();
		}
	}

	private PositiveInteger regenerateShield(RegenerateAction regenerateAction) {
		PositiveInteger actualRegen = calculateRegenValue(this.currentShieldHP, this.maxShieldHP,
				regenerateAction.shieldHPRegenerated);
		this.currentShieldHP = PositiveInteger.of(this.currentShieldHP.value() + actualRegen.value());
		return actualRegen;
	}

	private PositiveInteger regenerateHull(RegenerateAction regenerateAction) {
		PositiveInteger actualRegen = calculateRegenValue(this.currentHullHP, this.maxHullHP,
				regenerateAction.hullHPRegenerated);
		this.currentHullHP = PositiveInteger.of(this.currentHullHP.value() + actualRegen.value());
		return actualRegen;
	}

	private PositiveInteger calculateRegenValue(PositiveInteger currentValue, PositiveInteger maxValue,
			PositiveInteger regenValue) {
		int possibleRegen = regenValue.value();
		int neededRegen = maxValue.value() - currentValue.value();
		return PositiveInteger.of(Math.min(possibleRegen, neededRegen));
	}

	private int energyAfterUtilizingSubsystem(Subsystem subsystem) {
		return this.currentCapacitorAmount.value() - subsystem.getCapacitorConsumption().value();
	}

}
