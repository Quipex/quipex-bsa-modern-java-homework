package com.binary_studio.uniq_in_sorted_stream;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.filter(distinctById(Row::getPrimaryId));
	}

	private static <T> Predicate<T> distinctById(Function<T, Long> idExtractor) {
		Set<Long> seen = ConcurrentHashMap.newKeySet();
		// returns false if the value was already seen
		return t -> seen.add(idExtractor.apply(t));
	}

}
