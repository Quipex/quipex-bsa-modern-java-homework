package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Queue;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		Queue<Pair<Department, Integer>> queue = new LinkedList<>();
		if (rootDepartment == null) {
			return 0;
		}
		queue.add(Pair.of(rootDepartment, 1));

		int maxLevel = 0;
		while (!queue.isEmpty()) {
			Pair<Department, Integer> depToLevel = queue.poll();
			maxLevel = Math.max(maxLevel, depToLevel.getValue());

			for (Department subDepartment : depToLevel.getKey().subDepartments) {
				if (subDepartment != null) {
					queue.add(Pair.of(subDepartment, depToLevel.getValue() + 1));
				}
			}
		}
		return maxLevel;
	}

	private static class Pair<K, V> {

		private final K key;

		private final V value;

		Pair(K key, V value) {
			this.key = key;
			this.value = value;
		}

		public K getKey() {
			return this.key;
		}

		public V getValue() {
			return this.value;
		}

		public static <K, V> Pair<K, V> of(K key, V value) {
			return new Pair<>(key, value);
		}

	}

}
